# Prolog programming project — MPRI 2017 (2017-10-08) — Glen Mével


The Prolog source file can be loaded into SWI-Prolog’s interpreter with:

    ?- [dice].

(it assumes `library(time)` is loaded). This will provide predicates
`check_dice/1` for Part 1 of the project, `dice/3` for Part 2 and `maxdice/3`
for Part 3. All three predicates are deterministic. All three print the set of
dice. In addition, `maxdice/3` prints a line suitable for tabulating results,
including a timer.


## About Part 1


Given two dice D = (x_i) and E = (y_j), the probability that D beats E is:

    p = Score(D,E) / (|D|×|E|)

where we define the *score* as:

    Score(D,E) = card { (i,j) | x_i > y_j }

In a cycle of dice, we call a *level* the conjoint data of two consecutive dice.
A cycle of S-sided dice is non-transitive if and only if, at any level:

    Score > S² / 2


## About Part 2


A non-transitive dice cycle is impossible if N ⩽ 2 (obviously) or S ⩽ 2 (almost
obviously). If N ⩾ 3 and S ⩾ 3, then cycles can be generated from simple cases
and operations to combine them (which proves that such cycles exist for any N
and S). Generating a dice cycle (which happens to be optimal) for S ⩽ N+2 is
explained at the end of this documentation. This in particular gives solutions
for S = 3, S = 4 and S = 5, which we can use as a base. Here are exposed the two
operations to combine dice.

The first operation is **dice “dilation”**. Given a S-sided dice sequence:

    x_{1,1} … x_{1,S}
    x_{2,1} … x_{2,S}
    ⋮         ⋮
    x_{N,1} … x_{N,S}

We can construct a (K×S)-sided dice sequence of the same length N, for any
positive integer K, by repeating each side of each die K times:

    x_{1,1} … x_{1,1}  x_{1,2} … x_{1,2}  … … …  x_{1,S} … x_{1,S}
    x_{2,1} … x_{2,1}  x_{2,2} … x_{2,2}  … … …  x_{2,S} … x_{2,S}
    ⋮         ⋮        ⋮         ⋮               ⋮         ⋮
    x_{N,1} … x_{N,1}  x_{N,2} … x_{N,2}  … … …  x_{N,S} … x_{N,S}

It is easy to show that this preserves the probabilities of the original
sequence; in particular, this preserves non-transitivity. Indeed, for a given
level of the original dice, with beat probability p, the probability at the
corresponding level in the new (K×S)-sided dice sequence is:

    p’ = Score’ / (KS)² = K²×Score / (KS)² = Score / S² = p

The second operation is **dice concatenation**. Given two dice sequences
(x_{i,j}) with S₁ sides and (y_{i,j}) with S₂ sides, and assuming that all the
sides of the second set are greater than all the sides of the first set (without
loss of generality), we can concatenate the dice of each sequence pairwise:

    x_{1,1} … x_{1,S₁} y_{1,1} … y_{1,S₂}
    x_{2,1} … x_{2,S₁} y_{2,1} … y_{2,S₂}
    ⋮         ⋮        ⋮         ⋮
    x_{N,1} … x_{N,S₁} y_{N,1} … y_{N,S₂}

This gives lesser probabilities than the original sequences, but preserves
non-transitivity. Indeed, for a given level, where Score₁ (resp. Score₂) is the
score of the first (resp. second) sequence and Score’ the score of the
concatenation:

    Score’ = Score₁ + Score₂ + S₁×S₂ > S₁²/2 + S₂²/2 + S₁×S₂ = (S₁+S₂)² / 2


## About Part 3


`maxdice/3` relies on a conjecture that we did not prove, to restrict further
the search domain. It can be disabled by giving a fourth argument `Assumption`
set to 0 (setting it to 1 uses a weaker conjecture, 2 is the default).

This conjecture 2 considerably increases the speed, bringing it to practicable
time lapses. The results we get with it are consistent with those (much less
numerous) obtained without. Also, we did prove it for the special case S ⩽ N+2,
where we are able to give an explicit solution.

### Performance

Here are *all* the results obtained in less than 5 minutes, with no conjecture
(configuration: CPU Pentium® 2.30GHz, `x86_64`, SWI-Prolog 7.4.2):

     N|  S| time (s) | best minimal probability
    --+---+----------+-----------------
     3|  3|   0.020  |   5 /   9 ~ 0.56
     3|  4|   0.071  |   9 /  16 ~ 0.56
     3|  5|   0.410  |  15 /  25 ~ 0.60
     3|  6|   2.121  |  21 /  36 ~ 0.58
     3|  7|  15.526  |  29 /  49 ~ 0.59
     3|  8| 118.009  |  39 /  64 ~ 0.61
     3|  9| 506.101  |  48 /  81 ~ 0.59
    --+---+----------+-----------------
     4|  3|   0.029  |   5 /   9 ~ 0.56
     4|  4|   0.164  |  10 /  16 ~ 0.62
     4|  5|   1.146  |  16 /  25 ~ 0.64
     4|  6|   7.593  |  24 /  36 ~ 0.67
     4|  7|  93.725  |  31 /  49 ~ 0.63
     4|  8| 646.412  |  40 /  64 ~ 0.62
    --+---+----------+-----------------
     5|  3|   0.024  |   5 /   9 ~ 0.56
     5|  4|   0.277  |  10 /  16 ~ 0.62
     5|  5|   2.505  |  16 /  25 ~ 0.64
     5|  6|  22.186  |  24 /  36 ~ 0.67
     5|  7| 302.082  |  33 /  49 ~ 0.67
    --+---+----------+-----------------
     6|  3|   0.039  |   5 /   9 ~ 0.56
     6|  4|   0.480  |  10 /  16 ~ 0.62
     6|  5|   6.343  |  16 /  25 ~ 0.64
     6|  6|  85.753  |  24 /  36 ~ 0.67
    --+---+----------+-----------------
     7|  3|   0.049  |   5 /   9 ~ 0.56
     7|  4|   1.065  |  10 /  16 ~ 0.62
     7|  5|  18.282  |  16 /  25 ~ 0.64
     7|  6| 359.124  |  24 /  36 ~ 0.67
    --+---+----------+-----------------
     8|  3|   0.046  |   5 /   9 ~ 0.56
     8|  4|   1.759  |  10 /  16 ~ 0.62
     8|  5|  56.541  |  16 /  25 ~ 0.64
    --+---+----------+-----------------
     9|  3|   0.055  |   5 /   9 ~ 0.56
     9|  4|   3.623  |  10 /  16 ~ 0.62
     9|  5| 185.354  |  16 /  25 ~ 0.64
    --+---+----------+-----------------
    10|  3|   0.053  |   5 /   9 ~ 0.56
    10|  4|   8.437  |  10 /  16 ~ 0.62
    10|  5| 684.736  |  16 /  25 ~ 0.64
    --+---+----------+-----------------
    11|  3|   0.072  |   5 /   9 ~ 0.56
    11|  4|  21.288  |  10 /  16 ~ 0.62
    --+---+----------+-----------------
    12|  3|   0.066  |   5 /   9 ~ 0.56
    12|  4|  53.917  |  10 /  16 ~ 0.62

Here are some additional results we got with the conjecture:

     …
     3| 24|  32.909  | 352 / 576 ~ 0.61
     3| 25|  42.088  | 384 / 625 ~ 0.61
     3| 26|  62.880  | 416 / 676 ~ 0.62
     3| 27| 107.160  | 442 / 729 ~ 0.61
     3| 28| 154.559  | 478 / 784 ~ 0.61
     3| 29| 217.772  | 517 / 841 ~ 0.61
     3| 30| 334.819  | 551 / 900 ~ 0.61
    --+---+----------+-----------------
     4| 24|  10.279  | 384 / 576 ~ 0.67
     4| 25|  17.152  | 409 / 625 ~ 0.65
     4| 26|  26.103  | 442 / 676 ~ 0.65
     4| 27|  38.623  | 477 / 729 ~ 0.65
     4| 28|  48.917  | 518 / 784 ~ 0.66
     4| 29|  80.891  | 556 / 841 ~ 0.66
     4| 30| 104.495  | 600 / 900 ~ 0.67
    --+---+----------+-----------------
     5| 24|   5.899  | 396 / 576 ~ 0.69
     5| 25|   7.527  | 427 / 625 ~ 0.68
     5| 26|  12.161  | 460 / 676 ~ 0.68
     5| 27|  15.173  | 501 / 729 ~ 0.69
     5| 28|  23.871  | 537 / 784 ~ 0.68
     5| 29|  27.587  | 580 / 841 ~ 0.69
     5| 30|  42.301  | 615 / 900 ~ 0.68
     …

Interestingly, increasing N seams to *decrease* the computation time. This is
due to our representation for cycles: the domain of variables is O(S),
independent of N, and using the conjecture, the number of variables is also
O(S); moreover, the optimization problem is somewhat easier with greater values
of N (as, for instance, we were able to give explicitly an optimal answer for N
⩾ S-2).

### The representation of cycles

Given a sequence of dice, the first step to reduce the problem is to assume that
sides of each die are ordered. Then, we can draw an edge from each side of each
die to the right-most (that is, the maximal) side of the next die to which it is
superior. Of course, because sides are sorted, edges can’t cross each other.

For example, given the cycle of dice:

    Die1: 3 3 3 3 7 7
    Die2: 2 2 2 6 6 6
    Die3: 1 1 5 5 5 5
    Die4: 4 4 4 4 4 4

the graph would be:

      4 4 4 4 4 4
      ╰─┴─┴─┼─┴─╯    Score = 4+4+4+4+4+4
    0 3 3 3 3 7 7
      ╰─┴─┼─╯ ╰─┤    Score = 3+3+3+3 + 6+6
    0 2 2 2 6 6 6
      ╰─┼─╯ ╰─┴─┤    Score = 2+2+2 + 6+6+6
    0 1 1 5 5 5 5
    ╭─┴─╯ ╰─┴─┴─┤    Score = 0+0 + 6+6+6+6
    0 4 4 4 4 4 4

We assume that sides which are inferior to all sides of the following die point
to a special node called “0” (one “0” node per die).

As can be seen on the example above, giving only the graph is enough to compute
the associated scores: the scores of a level is simply the sum of its edges
(assuming that edges are coded as the index of their target side). Thus, moving
an edge to the right only increases the score.

Since it models a strict order, the graph is acyclic and thus is a forest.
Moreover, its roots are “0” nodes. In fact, a maximal cycle has only one root,
ie. it is a tree. [Indeed, if there were several roots, then we could pick one
of the roots, consider the edges pointing to it, and shift them to the right;
this would in effect attach the tree with that root to another tree, and
increase the score.]

Now, only giving such a tree is enough to build a cycle of dice whose graph is
this tree, ie. which observes the order induced by the tree: it suffices to
number the nodes in increasing order with a depth-first exploration of the tree.
Thus from now on, we will only be interested in generating such trees.

Furthermore, in a maximal cycle, all nodes of the tree at a given depth K point
to the same, right-most node of the tree at depth K-1, because again we can
transform any tree to that shape and that would only increase the scores.
Otherwise said, the structure of the tree is rather trivial: it is enough to
give its right-most node at each depth. We call this the *frontier* of the tree.

This is the case on the example above; its frontier is:

      4 4 4 4 4 4
            ╭───╯    Score = 6×4
    0 3 3 3 3 7 7
          ╭─╯   │    Score = 4×3 + 2×6
    0 2 2 2 6 6 6
        ╭─╯     │    Score = 3×2 + 3×6
    0 1 1 5 5 5 5
    ╭───╯       │    Score = 2×0 + 4×6
    0 4 4 4 4 4 4

Note by the way that, with such a shape, labeling the sides can be made simpler:
we simply give to all nodes of the tree at depth K the same value K.

Now the problem has been reduced. We can get rid of the remaining symetry by
stating that the root lies on the N-th die (for example).

Now the variables of the problem are the frontier nodes (coded as their index
within each die); except for the first one which is equal to 0, their domain is
1..S. The last N variables, and only them, are equal to S. In this example,
their sequence is:

    Tree = [0, 2, 3, 4, 6, 6, 6, 6]

In our Prolog program, we encode this by giving, for each die, the list of the
frontier nodes that it contains. In the example:

    Slices = [
        [        4,  6],
        [      3,    6],
        [    2,      6],
        [0,          6]
    ]

We can grossly bound the length of the sequence (that is, the depth of the tree)
by N×S. In practice, we fix the length at N×S because modeling it with `clpfd`
needs the set of variables to be known, and also to ensure that the list of
lists `Slices` is a rectangle. We add spurious values S if needed (this is fine
because it does not change the computed scores).

### Conjectures

At this point, we used conjectures to reduce further the search space, based on
observations on results we already had. We were not able to prove them simply.

**Conjecture 1**: We can assume that the tree frontier never decreases (ie there
is a maximal cycle with that property).

**Conjecture 2** (stronger): We can assume that the tree frontier increases
until it reaches its maximum (S), where it remains steady.

Conjecture 1 is already a great win, but Conjecture 2 is so strong that it
reduces the number of variables from O(N×S) to O(S) alone (because for K ⩽ S,
the K-th frontier node is at least K), and makes the computation time apparently
decreasing when N increases.

In practice, we fixed the length of `Tree` to the least multiple of N greater
than S+N (so that `Slices` is a rectangle). This is O(S) because we consider
that S > N+3, since we know how to treat the other case directly, as we shall
see shortly.

On all results obtained in practice, the depth of the tree is in fact almost
always under 2N+3 (including the last N frontier nodes which are equal to S).
This suggests a third conjecture that would still reduce the search space, but
its exact setting is not clear enough.

### Explicit solutions

Finally, we can treat some cases directly, because we were able to find an
explicit solution (which all satisfy the conjectures).

When S ⩽ N, it can be proven by a short mathematical study that a tree of that
shape is optimal — and is indeed non-transitive:

      x x x x x x
                │
               ...   ← Funnily enough, each die here beats the following die
                │      with probability 1.
      x x x x x x
                │
      x x x x x x
              ╭─╯
      x x x x x x
            ╭─╯ │
      x x x x x x
          ╭─╯   │
         ...   ...
        ╭─╯     │
      x x x x x x
      ╭─╯       │
      x x x x x x
    ╾─╯         │
      x x x x x x

This can be extended to the case where S = N+1.

      x x x x x x
            ╭─╯ │
      x x x x x x
          ╭─╯   │
         ...   ...
        ╭─╯     │
      x x x x x x
      ╭─╯       │
      x x x x x x
    ╾─╯       ╭─╯
      x x x x x x

And we can also prove that, for S = N+2, this is optimal:

      x x x x x x x x
                ╭───╯
      x x x x x x x x
              ╭─╯   │
      x x x x x x x x
            ╭─╯     │
           ...     ...
          ╭─╯       │
      x x x x x x x x
        ╭─╯         │
      x x x x x x x x
    ╾───╯           │
      x x x x x x x x

(However this last case has not been implemented in Prolog.)

Here are the proofs.

First, notice that for any tree that we consider, each column i ∈ 1..S has at
least one node whose edge points towards the left (ie. its target node is in a
column j < i), because trees are rooted to the left. We call the associated
level a cut for column i. One sees that, in order to maximize the score of such
a level, the edge must go left only by 1, and all other edges must point as far
right as possible. Otherwise said, this is the cut for i with the greatest
score:

      1       i       S
      ↓       ↓       ↓
      x x … x x x x … x
      ╰─┴─…─┼─╯ ╰─┴─…─┤    score Sc(i) = i×(i−1) + (S−i)×S
    0 x x … x x x x … x

Since any graph contains a cut for i, the minimal score of the graph satisfies
Score ⩽ Sc(i). This holds for all i, so Score ⩽ min_i Sc(i).

It turns out that for S ⩽ N, we can combine those optimal cuts to obtain the
tree drawed above. Hence this tree has Score = min_i Sc(i), which proves that it
is optimal. Moreover, we prove non-transitivity by a mathematical study. The
degree-two polynomial Sc(i) is maximal at i = 1 and i = S, and minimal at i =
S/2 when S is odd, or i = (S±1)/2) when S is even. The minimum value is

    Sc(s/2) = (S−1)×(3S+1) / 4 when S is odd,
    Sc((S±1)/2) = Sc(S/2) + ¼ when S is even,

which is greater than S²/2 for all S ⩾ 3.

Similarly, when S = N+1, there is at least one level which is a cut for two
columns i and j. Another quick analysis shows that the maximal such cut is
obtained with i = 1 and j = S, both going left by 1. The tree drawed above
precisely exploits this, hence is optimal too.

The same kind of analysis is enough to treat the case where S = N+2, and we
could probably go on forever that way, with more and more complicated studies,
but it doesn’t bring much. The case S ⩽ N+2 is interesting because it gives
solutions for all N when S ⩽ 5.
