%%  Prolog programming project
%%  MPRI 2017
%%  2017-10-08
%%  Glen Mével



%%
%% General functions, mainly about lists
%%



% Counts the answers to the goal.
count(Goal, N) :- findall(_, Goal, Li), length(Li, N).

% Builds the list of pairs of consecutive elements, ie.:
% pair_list([X1, …, Xn], [pair(X1,X2), …, pair(X{n-1},Xn)]).
pair_list([], []).
pair_list([X|Li], Pairs) :-
	pair_list_det(Li, X, Pairs).
pair_list_det([], _, []).
pair_list_det([Y|Li], X, [pair(X,Y)|Pairs]) :-
	pair_list_det(Li, Y, Pairs).

% The constant list of length N with element X.
constant_list(N, X, Li) :-
	dilate_list(N, [X], Li).

% Repeats each element of the list K times.
%dilate_list(0, _, []).
%dilate_list(1, Li, Li).
dilate_list(K, Li, Res) :-
	dilate_list_aux(Li, K, K, [], Res).
dilate_list_aux([], K, K, Acc, Res) :-
	reverse(Acc, Res).
dilate_list_aux([X|Li], K, I, Acc, Res) :-
	( I > 0 ->
		J is I-1,
		dilate_list_aux([X|Li], K, J, [X|Acc], Res)
	;
		dilate_list_aux(Li, K, K, Acc, Res)
	).

% Computes the list of differences of consecutive elements, ie.:
% delta_list(X0, [X1, …, Xn], [X1-X0, …, Xn-X{n-1}].
delta_list(X, Li, DLi) :-
	delta_list_det(Li, X, DLi).
delta_list_det([], _, []).
delta_list_det([Y|L], X, [Z|DL]) :-
	Z #= Y - X,
	delta_list_det(L, Y, DL).

% Computes a dichotomy of the input list; for example:
% dichot([1,2,3,4,5,6,7,8,9,10], [6,3,2,1,5,4,9,8,7,10]).
dichot([], []) :- !.
dichot(Li, [X|Di]) :-
	length(Li, N),
	M is N // 2,
	length(Mi, M),
	append(Mi, [X|Ri], Li),
	dichot(Mi, DiMi),
	dichot(Ri, DiRi),
	append(DiMi, DiRi, Di).



%%
%% 1. Check whether a sequence of dice is cyclic.
%%



% Computes the probability that Die1 beats Die2.
beat_proba(pair(Die1,Die2), P) :-
	count(
		(
			member(X, Die1),
			member(Y, Die2),
			X > Y
		),
		M
	),
	length(Die1, N1),
	length(Die2, N2),
	P is M rdiv (N1 * N2).

% If Dice = [Die1, …, Dien], computes the list of probabilities
% that Dien beats Die1, that Die1 beats Die2, …, and that Die{n-1} beats Dien.
% Also computes the minimum of those probabilities.
beat_probas_of_sequence(Dice, Probas, MinP) :-
	last(Dice, LastDie),
	DiceCycle = [ LastDie | Dice ],
	pair_list(DiceCycle, DicePairs),
	maplist(beat_proba, DicePairs, Probas),
	min_list(Probas, MinP).

% Print a sequence of dice, along with its associated probabilities.
print_dice(Dice) :-
	beat_probas_of_sequence(Dice, Probas, MinP),
	print_dice(Dice, Probas, MinP).
print_dice(Dice, Probas, MinP) :-
	forall(member(Die, Dice), format("  Die:  ~w~n", [Die])),
	format("  probabilities = ~w~n", [Probas]),
	MinPFloat is float(MinP),
	format("  minimum probability = ~w ~~ ~w~n", [MinP, MinPFloat]).

% Tests whether the sequence of dice is a non-transitive cycle,
% and prints it if this is the case.
check_dice(Dice) :-
	beat_probas_of_sequence(Dice, Probas, MinP),
	MinP > 1/2,
	print_dice(Dice, Probas, MinP).

% Tests with expected output:
% ?- check_dice([[2, 2, 4, 4, 9, 9], [1, 1, 6, 6, 8, 8], [3, 3, 5, 5, 7, 7]]).
% → true, [5/9, 5/9, 5/9]
% ?- check_dice([[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]]).
% → false
% ?- check_dice([
%	[ 7, 7, 7, 7, 7],
%	[ 6, 6, 6, 6, 6],
%	[ 5, 5, 5, 5, 5],
%	[ 4, 4, 4, 4,11],
%	[ 3, 3, 3,10,10],
%	[ 2, 2, 9, 9, 9],
%	[ 1, 8, 8, 8, 8]
%]).
% → true, [20/25, 1, 1, 20/25, 17/25, 16/25, 17/25]



%%
%% 2. Generation of a non-transitive dice cycle.
%%



:- use_module(library(clpfd)).


%% Procedures for combining dice sequences.


% Maps a sequence of S-sided dice to a sequence of (K×S)-sided dice;
% this preserves the minimal probability, thus non-transitivity.
dilate_dice(Dice, K, Res) :-
	maplist(dilate_list(K), Dice, Res).

% Maps a sequence of S1-sided dice and a sequence (of same length)
% of S2-sided dice to a sequence of (S1+S2)-sided dice;
% this preserves non-transitivity.
append_dice(Dice1, Dice2, Res) :-
	maplist(max_list, Dice1, Ms),
	max_list(Ms, M),
	% Note: this assumes that the sides of Dice2 have positive values.
	maplist(maplist(plus(M)), Dice2, Dice2shifted),
	maplist(append, Dice1, Dice2shifted, Res).


%% Generation of maximal dice cycles for certain cases.


% We are able to generate maximal dice cycles in certain cases.
% We will combine them to generate (possibly non-maximal) cycles
% for arbitrary N and S).
% Note: A non-transitive dice cycle exists if and only if N >= 3 and S >= 3.

% This one is obtained by the general procedure below:
%generate_maximal_cycle(3, 4, [
%	[ 3, 3, 3, 6 ],
%	[ 2, 2, 5, 5 ],
%	[ 1, 4, 4, 4 ]
%]) :- !.

generate_maximal_cycle(3, 5, [
	[ 3, 3, 3, 3, 3 ],
	[ 2, 2, 2, 5, 5 ],
	[ 1, 1, 4, 4, 4 ]
]) :- !.

% This one is obtained by the general procedure below:
%generate_maximal_cycle(4, 5, [
%	[ 4, 4, 4, 4, 8 ],
%	[ 3, 3, 3, 7, 7 ],
%	[ 2, 2, 6, 6, 6 ],
%	[ 1, 5, 5, 5, 5 ]
%]) :- !.

% If N >= S-1, generates a maximal cycle.
% See the attached documentation for an explanation.
generate_maximal_cycle(N, S, Dice) :-
	%N >= S,
	N >= S - 1,
	!,
	N >= 3,
	S >= 3,
	findall(Die,
		(
			between(1, N, I),
			%( I =< S ->
			( I < S ->
				J is S - I,
				K is I + N,
				constant_list(I, I, Pref),
				constant_list(J, K, Suff),
				append(Pref, Suff, Die)
			;
				constant_list(S, I, Die)
			)
		),
		RevDice
	),
	reverse(RevDice, Dice).


%% Generation of cycles for any (N,S).


% The idea is to obtain the required number S of sides by a linear combination
% of smaller values for which we can generate cycles directly.

% First method: by Euclidean division S = N Q + R, we can obtain S
% by combining N-sided dice (N = N, it fits in the special case above)
% and R-sided dice (most often R < N so generate_maximal_cycle applies too;
% however R = 1 and R = 2 are undoable, so we must pick R = N+1 and R = N+2
% in this case, for which we also know how to generate maximal cycles (but the
% case S = N+2 has not been implemented, so this procedure is partial)).
generate_cycle_1(N, S, Dice) :-
	N >= 3,
	S >= 3,
	Q is S // N,
	R is S mod N,
	( (R = 1 ; R = 2) ->
		K is Q - 1,
		J is R + N
	;
		K is Q,
		J is R
	),
	% invariant: S = K*N + J and J is neither 1 nor 2
	% (J = 0 or 2 < J < N or J = N+1 or J = N+2).
	generate_maximal_cycle(N, N, Dice1),
	dilate_dice(Dice1, K, KDice1),
	( J = 0 ->
		constant_list(N, [], Dice2)
	;
		generate_maximal_cycle(N, J, Dice2)
	),
	append_dice(KDice1, Dice2, Dice).

% Second method: same idea, but dividing by 3 instead of N, since we know
% how to generate 3-sided dice cycles of any length. For the same reason
% as above, we need to be able to generate a 4-sided and 5-sided dice cycles
% as well (which we are).
generate_cycle_2(N, S, Dice) :-
	N >= 3,
	S >= 3,
	Q is S // 3,
	R is S mod 3,
	( R \= 0 ->
		K is Q - 1,
		J is R + 3
	;
		K is Q,
		J is R
	),
	% invariant: S = K*3 + J and J is 0, 4 or 5.
	generate_maximal_cycle(N, 3, Dice1),
	dilate_dice(Dice1, K, KDice1),
	( J = 0 ->
		constant_list(N, [], Dice2)
	;
		generate_maximal_cycle(N, J, Dice2)
	),
	append_dice(KDice1, Dice2, Dice).

% The method chosen:
dice(N, S, Dice) :-
	generate_cycle_2(N, S, Dice),
	check_dice(Dice).



%%
%% 3. Generation of a maximal cycle.
%%



% The property that the list remains constant once it has reached Max.
steady_once_at_max(Max, Li) :-
	pair_list(Li, Pairs),
	maplist({Max}/[pair(X,Y)] >> ((X #= Max) #==> (Y #= Max)), Pairs).

% The property that the list is increasing until Max is reached,
% in which case it remains steady.
increasing_until_max(Max, Li) :-
	chain(Li, #=<),
	pair_list(Li, Pairs),
	maplist({Max}/[pair(X,Y)] >> ((X #= Y) #<==> (X #= Max)), Pairs).

% Computes the probability that Die1 beats Die2, when the dice cycle is
% represented by a tree (see attached documentation).
% More exactly, computes the score, that is, the probability without
% the normalization factor S².
slices_beat_score(pair(Slice1,Slice2), Score) :-
	delta_list(0, Slice1, DeltaSlice1),
	maplist([A,B,C] >> (A*B #= C), DeltaSlice1, Slice2, ProdList),
	sum(ProdList, #=, Score).

% Modelization of the problem, with the dice cycle represented
% as a tree’s frontier (see documentation).
% `Tree` is the tree frontier, as a list.
% `Slices` is that frontier, split over the dices: the k-th slice is the list
% of elements of the frontier that are on the k-th die.
% `MinScore` is the minimal score of the cycle.
% The parameter `Assumption` can be set to:
%   + 0 for no additional assumption,
%   + 1 to use the assumption that the tree’s frontier is non-decreasing,
%   + 2 to use the assumption that the tree’s frontier is increasing.
dice_core(N, S, Tree, Slices, MinScore) :-
	dice_core(N, S, Tree, Slices, MinScore, 2).
dice_core(N, S, Tree, Slices, MinScore, Assumption) :-
	% global requirement
	N >= 3,
	S >= 3,
	% appropriate size
	length(Slices, N),
	( Assumption = 2 ->
		LayerBound is (S + N - 1) // N + 1
	;
		LayerBound is S
	),
	maplist({LayerBound}/[Slice] >> length(Slice, LayerBound), Slices),
	% appropriate domain
	maplist({S}/[Slice] >> (Slice ins 0..S), Slices),
	% slices are ordered (~ symetry breaking)
	maplist({S}/[Slice] >> increasing_until_max(S, Slice), Slices),
	% the tree roots at the first die (symetry breaking)
	once(append(OtherSlices, [ [0|LastSliceTail] ], Slices)),
	maplist([ [FirstSide|_] ] >> (FirstSide #> 0), OtherSlices),
	% the tree covers all sides of all dice
	maplist({S}/[Slice] >> last(Slice, S), Slices),
	% recover the whole tree’s frontier
	reverse(Slices, RevSlices),
	transpose(RevSlices, TreeLayers),
	append(TreeLayers, Tree),
	% ordering of the tree’s frontier
	( Assumption = 2 ->
		increasing_until_max(S, Tree)
	; Assumption = 1 ->
		chain(Tree, #=<)
	;
		steady_once_at_max(S, Tree)
	),
	% compute scores
	append(LastSliceTail, [S], LastSliceShifted),
	SlicesCycle = [ LastSliceShifted | Slices ],
	pair_list(SlicesCycle, SlicePairs),
	maplist(slices_beat_score, SlicePairs, Scores),
	% each die beats the next die
	S2 is S * S,
	ScoreThreshold is S2 // 2,
	maplist({ScoreThreshold}/[Score] >> (Score #> ScoreThreshold), Scores),
	% gives the criterion to maximize: the minimum MinScore of the list Scores
	foldl([A,B,C] >> (min(A,B) #= C), Scores, S2, MinScore),
	% redundant constraints
	% - lower bounds for levels:
	( Assumption = 2 ->
		length(Tree, TreeLength),
		LastTreeIndex is TreeLength - 1,
		numlist(0, LastTreeIndex, TreeIndex),
		maplist({S}/[I,Lvl] >> (Lvl #>= min(I,S)), TreeIndex, Tree)
	; true
	),
	% - upper bounds for probabilities:
	maplist({S2}/[Score] >> (Score #=< S2), Scores),
	true.

% The final predicate.
% `Backtracking` enables backtracking; it is normally not useful anymore,
% but would be if we were unable to get the maximum directly with `labeling`.
%maxdice(N, S, Dice) :-
%	% Short-circuit for special cases.
%	generate_maximal_cycle(N, S, Dice),
%	!,
%	print_dice(Dice).
maxdice(N, S, Dice) :-
	maxdice(N, S, Dice, false).
maxdice(N, S, Dice, Backtracking) :-
	( Backtracking ->
		retractall(best_MinScore_so_far(_)),
		assert(best_MinScore_so_far(0))
	;
		true
	),
	statistics(cputime, Time0),
	% Model the problem.
	dice_core(N, S, Tree, Slices, MinScoreExpr),
	% Instanciate the dice, maximizing MinScore.
	% Several orderings for variables to give to `labeling` were tried,
	% they are ranged here from worst to best performance.
	%Vars = Tree, % #1
	%Len is S//2,length(M,Len),append(M,L,Tree),reverse(M,RevM),append(RevM,L,Vars), % #2
	%reverse(Tree, Vars), % #3
	%dichot(Tree, Vars), % #4
	%append(Slices, Vars), % #5
	reverse(Slices, RevSlices), append(RevSlices, Vars), % #6
	% Same thing for selection strategies.
	%labeling([down,max,max(MinScoreExpr)], Vars), % #1
	labeling([down,max(MinScoreExpr)], Vars), % #2
	% Conclude.
	statistics(cputime, Time1),
	MinScore is MinScoreExpr,
	( Backtracking ->
		best_MinScore_so_far(BestMinScore),
		MinScore > BestMinScore,
		retractall(best_MinScore_so_far(_)),
		assert(best_MinScore_so_far(MinScore))
	;
		!
	),
	% Decoding.
	decode_treefrontier(N, S, Slices, Dice),
	% Printing.
	print_dice(Dice),
	S2 is S * S,
	format("| ~|~` t~d~3+ | ~|~` t~d~3+ | ~|~` t~3f~8+ | ~|~` t~d~4+ / ~|~` t~d~4+ ~~ ~2f |~n", [N, S, Time1 - Time0, MinScore, S2, MinScore / S2]),
	true.

% Decodes the representation of a dice cycle by its tree’s frontier.
decode_treefrontier(N, _S, Slices, Dice) :-
	LastSliceIndex is N-1,
	numlist(0, LastSliceIndex, SlicesIndexes),
	reverse(SlicesIndexes, RevSlicesIndexes),
	maplist(
		{N} / [SliceIndex, Slice, Die] >> (
			length(Slice, NbLayers),
			LastLayerIndex is NbLayers-1,
			numlist(0, LastLayerIndex, LayersIndexes),
			delta_list(0, Slice, DeltaSlice),
			maplist(
				{N, SliceIndex} / [LayerIndex, D, DiePart] >> (
					SideValue is SliceIndex + N * LayerIndex,
					constant_list(D, SideValue, DiePart)
				),
				LayersIndexes, DeltaSlice, DieParts
			),
			append(DieParts, Die)
		),
		RevSlicesIndexes, Slices, Dice
	).
